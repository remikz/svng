from PyQt4.QtCore import * 
from PyQt4.QtGui import * 
import sys

'''
http://stackoverflow.com/questions/31342228/pyqt-tree-widget-adding-check-boxes-for-dynamic-removal#31342831
'''

def walkSelected(tree):
    iterator = QTreeWidgetItemIterator(tree, QTreeWidgetItemIterator.Checked)
    paths = []
    while iterator.value():
        item = iterator.value()
        text = item.data( 0, Qt.UserRole ).toString()
        if text: paths.append( str(text) )
        iterator += 1
    print '\n'.join( paths )        
        
def main(): 
    app     = QApplication(sys.argv)
    tree    = QTreeWidget()
    tree.setHeaderLabels( ['Path', 'Date', 'Status', 'Revision', 'User', 'Size'] )
 
    for i in xrange(3):
        parent = QTreeWidgetItem(tree)
        parent.setText(0, "Parent {}".format(i))
        parent.setFlags(parent.flags() | Qt.ItemIsTristate | Qt.ItemIsUserCheckable)
        for x in xrange(5,1,-1):
            child = QTreeWidgetItem(parent)
            child.setFlags(child.flags() | Qt.ItemIsTristate | Qt.ItemIsUserCheckable)
            child.setText(0, "Child {}".format(x))
            child.setCheckState(0, Qt.Unchecked)
            for y in xrange(2):
                leaf = QTreeWidgetItem(child)
                leaf.setFlags(leaf.flags() | Qt.ItemIsUserCheckable)
                leaf.setText(0, "Leaf {}".format(y))
                leaf.setCheckState(0, Qt.Unchecked)
                parts = [ str(i.text(0)) for i in [parent, child, leaf ]]
                path = '/'.join(parts)
                leaf.setData( 0, Qt.UserRole, path )
                
    tree.resizeColumnToContents( 0 )
    tree.setSortingEnabled( True )
    tree.show() 
    rcode = app.exec_() 
    walkSelected( tree )
    return rcode
    
if __name__ == '__main__':
    sys.exit( main() )
 