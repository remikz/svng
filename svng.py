#!/usr/bin/env python
'''
TODO
- Persist settings: theme selected.
'''
import os
import subprocess
import sys

from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import QDialog
  
from datetime import datetime
from dateutil import tz

from_zone = tz.tzutc()
to_zone = tz.tzlocal()

#====================================================================
def commafyNumber(v):
  return "{:,}".format(v)

#====================================================================
def normalizePath(path):
  '''Convert to unix slashes.'''
  return os.path.normpath( path ).replace( '\\', '/' ).replace('//','/')

#====================================================================
def printDict(d, indent=0):
  if type(d) == dict:
    for k,v in d.items():
      print '{0}{1}:'.format( indent*' ', k )
      printDict( v, indent+4 )
  else:
    print '{0}{1}'.format( indent*' ', d )

#====================================================================
def system(c, block=True):
  '''Run shell command.
     Optionally block and wait for subprocess to finish.

     Return stdout,errorCode '''
  p = subprocess.Popen(c, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
  if block and p.stdout:
    return p.communicate()[0], p.returncode # Waits for it to terminate.
  else:
    return '', p.returncode

#====================================================================
def utcToLocal(dtStr):
  utc = datetime.strptime( dtStr, '%Y-%m-%dT%H:%M:%S.%fZ')
  utc = utc.replace( tzinfo=from_zone )
  return utc.astimezone( to_zone ).strftime( '%Y-%m-%d %H:%M:%S' )

#====================================================================
class Tools(object):
  @staticmethod
  def cdUp(path):
    return '/'.join( normalizePath( path ).split('/')[:-1] )
    
  @staticmethod
  def findSvnDir(start):
    '''Traverse up parent dirs to find first dir with .svn/ subdir.'''
    d = start
    while d:
      found = [d for o in os.listdir(d) if o=='.svn' and os.path.isdir(os.path.join(d,o))]
      if found:
        return found[0]
      d = Tools.cdUp(d)
      
    return None
        
  @staticmethod
  def getMonoFont():
    font = QtGui.QFont( "Monospace" )
    font.setStyleHint( QtGui.QFont.TypeWriter )
    return font
    
  @staticmethod
  def indent(depth, *args):
    sys.stdout.write( depth*'  ' )
    [sys.stdout.write( str(arg) ) for arg in args]
    print
    
  @staticmethod
  def remove(paths):
    import shutil
    out = ''

    def handleErrors(func, path, exc_info, out=out):
      out += 'Error: failed to remove ' + path + '\n'

    for path in paths:
      if os.path.isdir( path ):
        shutil.rmtree( path, onerror=handleErrors )
      else:
        try:
          os.remove( path )
        except:
          handleErrors( None, path, None )

      if path not in out:
        out += 'Removed ' + path + '\n'

    return out

#====================================================================
class CommitWindow(QDialog):
  def __init__(self, parent=None):
    super(CommitWindow, self).__init__()
    self.win = parent
    self.initUi()

  def groupsIndexChanged(self, index):
    text = self.groups.currentText()
    if text == 'All':
      ids = self.win.svnTree.visibleIds()
    elif text == 'Checked':
      ids = self.win.svnTree.checkedIds()
    elif text == 'Selected':
      ids = self.win.svnTree.selectedIds()
    else:
      ids = self.win.groups.group( text ).getIds()
      
    self.items = App.instance.model.getItemsById( ids )    
    self.setItems( self.items )   
    
  def initUi(self):
    self.initGroups()
    self.initMessages()
    self.textEdit = QtGui.QTextEdit()
    self.list = QtGui.QTextEdit()
    self.okButton = QtGui.QPushButton('OK')
    self.cancelButton = QtGui.QPushButton('Cancel')

    font = Tools.getMonoFont()
    self.textEdit.setFont( font )
    self.list.setFont( font )
    
    self.groups.currentIndexChanged.connect( self.groupsIndexChanged )
    self.messages.currentIndexChanged.connect( self.messagesIndexChanged )
    self.okButton.clicked.connect( self.okClicked )
    self.cancelButton.clicked.connect( self.cancelClicked )

    hbox = QtGui.QHBoxLayout()
    hbox.addStretch(1)
    hbox.addWidget( self.okButton )
    hbox.addWidget( self.cancelButton )

    grid = QtGui.QGridLayout()
    grid.setSpacing( 10 )
    grid.addWidget( self.groups,   0,0,  1,1 )
    grid.addWidget( self.messages, 1,0,  1,1 )
    grid.addWidget( self.textEdit, 2,0,  3,1 )
    grid.addWidget( self.list,     5,0,  5,1 )
    grid.setRowStretch(            5, 1 )
    grid.addLayout( hbox,          10,-1, 1,1, QtCore.Qt.AlignRight )

    self.setLayout( grid )
    center = self.win.geometry().center()
    w,h = 800, 800
    self.setGeometry( center.x()-w/2, center.y()-h/2, w, h )
    self.setWindowTitle( 'Commit' )

    self.groupsIndexChanged( 0 )
    
  def initGroups(self):
    self.groups = QtGui.QComboBox()
    self.groups.addItem( 'All' )    
    self.groups.addItem( 'Checked' )    
    self.groups.addItem( 'Selected' )
  
    names = self.win.groups.getNames()  
    if names:
      self.groups.insertSeparator(99)
    
    for name in names:
      self.groups.addItem( name )
    
  def initMessages(self):
    self.messages = QtGui.QComboBox()
    msgs = App.instance.cfg.commits.getMessages()

    self.messages.addItem( '' )
    for msg in msgs:
      self.messages.addItem( msg )
   
  def messagesIndexChanged(self, index):
    self.textEdit.setText( self.messages.currentText() )
    
  def cancelClicked(self):
    self.items = []
    self.reject()
    
  def okClicked(self):
    if not self.textEdit.toPlainText():
      QtGui.QMessageBox.critical( None, 'Commit Error', 'Message required.' )
      self.textEdit.setFocus()
      return

    self.accept()

  def setItems(self, items):
    text = ''
    count = 0
    paths = [item.path for item in items]
    paths = sorted( App.instance.model.stripCommonPrefix( paths ) )
    maxNumLen = len(str(len(paths)))
    
    for path in paths:
      if not path:
        continue
      
      count += 1
      pad = (maxNumLen - len(str(count)) + 2) * ' '
      text += '{0}{1}{2}\n'.format( count, pad, path )

    self.list.setText( text )
    self.setWindowTitle( 'Commit {0} Files'.format( count ) )

#====================================================================
class Svn(object):
  def __init__(self):
    self.model = None

  def setModel(self, model):
    self.model = model

  def add(self, paths):
    arg = ' '.join( paths )
    cmd = 'svn add {0}'.format( arg )
    return system( cmd )

  def commit(self, paths, comment=''):
    if not comment:
      return 'svn Error: Commit message missing. Aborted.', 0

    # Wrap if @ is in name.
    wrapped = []
    for path in paths:
      if '@' in path and not path.endswith('@'):
        wrapped.append( '"{0}@"'.format( path ) )
      else:
        wrapped.append( path )

    arg = ' '.join( wrapped )
    cmd = 'svn commit -m "{0}" {1}'.format( comment, arg )
    return system( cmd )

  def delete(self, paths):
    arg = ' '.join( paths )
    cmd = 'svn delete --force {0}'.format( arg )
    return system( cmd )

  def diff(self, paths):
    pass

  def ignore(self, paths):
    pass

  def meld(self, paths):
    pass

  def revert(self, paths):
    print 'svn revert ' + ' '.join( paths )

  def update(self, paths):
    arg = ' '.join( paths )
    cmd = 'svn update {0}'.format( arg )
    return system( cmd )

#====================================================================
class SvnStatus(object):
  MAP = {
    'ADDED': 'A',
    'CONFLICTED': 'C',
    'DELETED': 'D',
    'IGNORED': 'I',
    'MISSING': '!',
    'MODIFIED': 'M',
    'NOMODS': ' ',
    'OUTDATED': '*',
    'REPLACED': 'R',
    'UNDEFINED': '0',
    'UNVERSIONED': '?'
  }

  @staticmethod
  def toCode(text):
    if not text or not len(text):
      return SvnStatus.UNDEFINED
    return SvnStatus.MAP.get( text[0], SvnStatus.UNDEFINED )

  @staticmethod
  def toString(code):
    for k,v in SvnStatus.MAP:
      if v == code:
        return k
    return 'UNDEFINED'

#====================================================================
class SvnItem(object):
  def __init__(self, path=None, rev=None, remoteRev=None, status=None, remoteStatus=None, user=None, size=None, date=None, id=None):
    self.rev = rev
    self.remoteRev = remoteRev
    self.status = status
    self.remoteStatus = remoteStatus
    self.date = date
    self.path = path
    self.size = size
    self.user = user
    self.id = id

  @staticmethod
  def isGloballyUnmodified(text):
    return len(text) > 8 and text[0] == ' ' and text[8] == ' '

  @staticmethod
  def createFromText(text):
    item = SvnItem()
    item.path = SvnItem.parsePath( text )
    item.rev = SvnItem.parseRev( text )
    item.remoteRev = SvnItem.parseRemoteRev( text )
    item.status = SvnItem.parseStatus( text )
    item.remoteStatus = SvnItem.parseRemoteStatus( text )
    item.user = SvnItem.parseUser( text )
    return item

  def isUnversioned(self):
    return self.status.lower().startswith( 'unversioned' )

  @staticmethod
  def parsePath(text):
    split = text[10:].split(' ', 4)
    path = normalizePath( split[-1] )
    return path

  @staticmethod
  def parseRemoteRev(text):
    return text[10:].split( ' ', 2 )[1]

  @staticmethod
  def parseRev(text):
    return text[10:].split( ' ', 1 )[0]

  @staticmethod
  def parseStatus(text):
    if not text or not len(text):
      return SvnStatus.MAP['UNDEFINED']
    return text[0]

  @staticmethod
  def parseRemoteStatus(text):
    if len(text) > 7 and text[8] == '*':
      return '*'
    return ' '

  @staticmethod
  def parseTextBlock(block, skipUnmodified=True):
    items = []
    for text in block.split('\n'):
      if text.rstrip():
        if skipUnmodified and SvnItem.isGloballyUnmodified( text ):
          continue
        items.append( SvnItem.createFromText( text ) )
    return items

  @staticmethod
  def parseUser(text):
    return text[10:].split( ' ', 3 )[2]

  def __repr__(self):
    fmt = '''SvnItem({0}, {1}, {2}, {3}, {4}, {5})'''
    return fmt.format( repr(self.path), repr(self.rev), repr(self.status), repr(self.user), repr(self.size), repr(self.date) )

#====================================================================
class PathTreeBuilder(object):    
  @staticmethod
  def add_(tree, split, item, depth=0):
    head, tail = split[0], split[1:]
    isLeaf = not tail
    #Tools.indent( depth, 'head:', head )
    #Tools.indent( depth, 'tail:', tail )
    #Tools.indent( depth, 'item:', item )
    if isLeaf:
      if not os.path.isdir( item.path ):
        tree[head] = item
      return

    if len(tail) == 1 and tail[0] not in tree:
      #Tools.indent( depth, 'key:', tail[0] )
      #Tools.indent( depth, 'item:', item )
      tree[tail[0]] = item
      return

    subtree = tree.get( head, None )
    if subtree is None:
      subtree = {}
      #Tools.indent( depth, 'head:', head )
      #Tools.indent( depth, 'subtree:', subtree )
      tree[head] = subtree

    PathTreeBuilder.add_( subtree, tail, item, depth+1 )

  @staticmethod
  def build(commonpath, items):
    tree = {}
    commonpath = normalizePath( commonpath )
    for item in items:
      path = normalizePath( item.path )
      if path.startswith( commonpath ):
        # Remove common prefix to keep compact.
        path = path[ len(commonpath): ]
        if path.startswith('/'):
          path = path[1:]
      split = path.split( '/' )
      PathTreeBuilder.add_( tree, split, item )

    return tree

  @staticmethod
  def findChildrenStatus(children):
    '''Use to find any status of children to colorize parent dir.
       Could improve with majority of histogram.
    '''
    if type(children) == SvnItem:
      return children.status
    
    for child in children:
      status = PathTreeBuilder.findChildrenStatus( child )
      if status:
        return status
        
    return None
    
#====================================================================
class SvnXmlParser(object):
  @staticmethod
  def parse(text):
    from xml.etree import cElementTree as ET
    items = []
    root = ET.fromstring( text )
    for el in root[0][:-1]:
      path = normalizePath( el.attrib['path'] )
      if os.path.isdir( path ):
        path += '/'
        isFile = False
      else:
        isFile = os.path.isfile( path )

      localStatus = el[0].attrib.get('item', None)
      localRev    = el[0].attrib.get('revision', None)
      size        = os.path.getsize( path ) if isFile else None
      remoteRev, user, date, remoteStatus = None, None, None, None
      if len(el[0]):
        remoteRev = el[0][0].attrib.get('revision', None)
        user = el[0][0][0].text
        date = el[0][0][1].text
      if len(el) > 1:
        remoteStatus = el[1].attrib.get('item', None)

      item = SvnItem( path=path, rev=localRev, remoteRev=remoteRev, status=localStatus, remoteStatus=remoteStatus, user=user, size=size, date=date, id=len(items) )
      items.append( item )
    return items
    
#====================================================================
class NewNameDialog(QDialog):  
  def __init__(self, parent, title):
    super( NewNameDialog, self ).__init__()
    
    self.name = QtGui.QLineEdit()
    
    self.cancel = QtGui.QPushButton('Cancel')
    self.cancel.clicked.connect( self.reject )

    self.ok = QtGui.QPushButton('Ok')
    self.ok.clicked.connect( self.accept )

    label = QtGui.QLabel( "Name:" )
    
    layout = QtGui.QGridLayout()
    layout.addWidget(label,       0,0, 1,3 )
    layout.addWidget(self.name,   1,0, 1,3 )
    layout.addWidget(self.cancel, 2,1, 1,1 )
    layout.addWidget(self.ok,     2,2, 1,1 )
    layout.setColumnStretch( 0, 1 )
    layout.setRowStretch( 3, 1 )
    self.setLayout(layout)

    center = parent.geometry().center()
    w,h = 200, 100
    self.setGeometry( center.x()-w/2, center.y()-h/2, w, h )
    self.setWindowTitle( title )
    
#====================================================================
class ItemListView(QtGui.QListWidget):
  _sizehint = None
  
  def __init__(self, parent):
    super( ItemListView, self ).__init__( parent )
    self.items = [] 
    self.setColor()
  
  def add(self, items):
    self.items.extend( items )
    self.updateView()
    
  def getItems(self):
    raise NotImplementedError
    
  def setColor(self):
    p = self.palette()
    bg = ColorConfig.hexToQColor( App.instance.cfg.colors.getTheme()['general']['bg'] )
    p.setColor( QtGui.QPalette.Base, bg )
    self.setPalette( p )

    fg = ColorConfig.hexToQColor( App.instance.cfg.colors.getTheme()['general']['fg'] )
    for i in range( self.count() ):
      self.item( i ).setForeground( fg );

  def setSizeHint(self, width, height):
    self._sizehint = QtCore.QSize(width, height)

  def sizeHint(self):
    if self._sizehint is not None:
      return self._sizehint
    return super( ItemListView, self ).sizeHint()

  def updateView(self):
    self.clear()
    self.addItems( self.getItems() )
    self.setColor()

#====================================================================
class SvnItemListView(ItemListView):
  def __init__(self, parent):
    super( SvnItemListView, self ).__init__( parent )
    self.items = [] 
  
  def getIds(self):
    return [item.id for item in self.items]
    
  def getItems(self):
    paths = [item.path for item in self.items]
    paths.sort()
    
    return App.instance.model.stripCommonPrefix( paths )
       
#====================================================================
class GroupView(SvnItemListView):
  def __init__(self, win, name):
    super( GroupView, self ).__init__( win )
    self.win = win  
    self.name = name
  
#====================================================================
class Groups(object):
  '''Manages many GroupView objects.'''
  def __init__(self, win):
    self.win = win  
    self.groups = {} # name->dock
  
  def add(self, name, item):
    self.groups[name].widget().add( item )
    
  def askForName(self, event):
    dlg = NewNameDialog( self.win, 'New Group' )
    dlg.exec_()
    
    name = dlg.name.text()
    if not name:
      return
    
    dock = QtGui.QDockWidget( name, self.win )
    view = GroupView( self.win, name )
    dock.setWidget( view )
    dock.setVisible( True )
    self.win.addDockWidget( QtCore.Qt.BottomDockWidgetArea, dock )    
    self.groups[ name ] = dock
    
  def group(self, name):
    return self.groups[name].widget()
    
  def getNames(self):
    return sorted( self.groups.keys() )
    
#====================================================================
class Model(object):
  def __init__(self, path, isRemote=False):
    self.path = path
    self.isRemote = isRemote
    self.items = [] # SvnItem list.
    self.refresh()

  def countUnversioned(self):
    count = 0
    for item in self.items:
      if item.isUnversioned():
        count += 1
    return count

  def filteredItems(self):
    '''Return items that are not ignored by config.'''
    result = []
    for item in self.items:
      if not App.instance.cfg.ignores.isIgnorable( item.path ):
        result.append( item )
    return result

  def getItemsById(self, ids):
    idSet = set(ids)
    result = [item for item in self.items if item.id in idSet]
    return result

  def ignoredItems(self):
    '''Return items filtered out and reasons.'''
    result = []
    for item in self.items:
      rule = App.instance.cfg.ignores.isIgnorable( item.path )
      if rule:
        result.append( (item.path, rule) )
        
    return result
    
  def isEmpty(self):
    return len(self.items) == 0

  def refresh(self):
    if self.isRemote:
      App.instance.showMessage( 'Fetching from server.' )

    remoteOption = '--show-updates' if self.isRemote else ''
    cmd = 'svn st --xml {0} {1}'.format( remoteOption, self.path )
    App.instance.showMessage( '' )
    stdout, rcode = system( cmd )
    self.items = SvnXmlParser.parse( stdout )

  def setRemote(self, val):
    self.isRemote = val

  def stripCommonPrefix(self, paths):
    prefixLen = len(self.path)
    result = []
    for i in range(len(paths)):
      path = paths[i][ prefixLen: ].strip()
      if path and path[0] == '/':
        path = path[1:]
        
      result.append( path )
      
    return result

#====================================================================
class SvnTreeWidgetContextMenuHandler(object):
  def __init__(self, svnTree):
    self.svnTree = svnTree

  def show(self, event):
    self.menu = QtGui.QMenu( self.svnTree )

    action = QtGui.QAction( 'Diff Checked', self.svnTree )
    action.triggered.connect( lambda: self.diffCheckedSlot(event) )
    self.menu.addAction( action )

    action = QtGui.QAction( 'Diff', self.svnTree )
    action.triggered.connect( lambda: self.diffSelectedSlot(event) )
    self.menu.addAction( action )

    action = QtGui.QAction( 'Meld', self.svnTree )
    action.triggered.connect( lambda: self.meldSelectedSlot(event) )
    self.menu.addAction( action )

    #------------------------------------------------------
    self.menu.addSeparator()

    action = QtGui.QAction( 'Expand', self.svnTree)
    action.triggered.connect( self.svnTree.expandSelected )
    self.menu.addAction( action )

    action = QtGui.QAction( 'Collapse', self.svnTree)
    action.triggered.connect( self.svnTree.collapseSelected )
    self.menu.addAction( action )

    #------------------------------------------------------
    self.menu.addSeparator()

    action = QtGui.QAction( 'Delete', self.svnTree)
    action.triggered.connect( self.svnTree.deleteSelected )
    self.menu.addAction( action )

    action = QtGui.QAction( 'Revert', self.svnTree)
    action.triggered.connect( self.svnTree.revertSelected )
    self.menu.addAction( action )

    #------------------------------------------------------
    self.menu.addSeparator()

    action = QtGui.QAction( 'Open', self.svnTree)
    action.triggered.connect( self.svnTree.openPath )
    self.menu.addAction( action )

    #------------------------------------------------------
    self.menu.addSeparator()

    action = QtGui.QAction( 'Copy Paths', self.svnTree)
    action.triggered.connect( self.svnTree.copyPathsToClipboard )
    self.menu.addAction( action )

    #------------------------------------------------------
    self.menu.addSeparator()
    self.addGroupActions()

    #------------------------------------------------------
    self.menu.popup( QtGui.QCursor.pos() )

  def addGroupActions(self):
    for name in App.instance.win.groups.getNames():
     action = QtGui.QAction( 'Move to Group: {0}'.format(name), self.svnTree )
     action.setData( name )
     action.triggered.connect( self.svnTree.moveToGroup )
     self.menu.addAction( action )    

  def diffCheckedSlot(self, event):
    self.svnTree.diffChecked()

  def diffSelectedSlot(self, event):
    self.svnTree.diffSelected()

  def meldSelectedSlot(self, event):
    self.svnTree.meldSelected()

#====================================================================
class SvnTreeWidget(QtGui.QTreeWidget):
  # Columns.
  NAME = 0
  STATUS = 1
  REV  = 2
  REMOTE_REV = 3
  USER = 4
  SIZE = 5
  DATE = 6
  PATH = 7
  COLUMN_COUNT = 8

  def __init__(self, parent):
    super( SvnTreeWidget, self ).__init__( parent )
    self.contextMenu = SvnTreeWidgetContextMenuHandler( self )
    self.flat = False
    self.setHeaderLabels( self.labels() )
    self.setSortingEnabled( True )
    self.model = None
    self.itemDoubleClicked.connect( self.doubleClickedSlot )
    self.setSelectionMode( self.ExtendedSelection )
    self.setColor()
    self.svn = None
    self.win = parent

  @staticmethod
  def labels():
    return ['Name', 'Status', 'Local', 'Remote', 'User', 'Size', 'Date', 'Path']

  def setModel(self, model):
    self.model = model

  def setSvn(self, svn):
    self.svn = svn

  def build(self):
    if self.flat:
      self.buildFlat()
    else:
      self.buildTree()

  def buildRow(self, data, item):
    if data.status: item.setText( self.STATUS, data.status )
    if data.rev and data.rev != '-1':  item.setText( self.REV, data.rev )
    if data.remoteRev: item.setText( self.REMOTE_REV, data.remoteRev )
    if data.user: item.setText( self.USER, data.user )

    if data.size:
      item.setText( self.SIZE, commafyNumber(int(data.size)) )
      item.setTextAlignment( self.SIZE, QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter );

    if data.date: item.setText( self.DATE, utcToLocal( data.date ) )
    if data.path: item.setText( self.PATH, data.path )

    font = item.font( 0 )
    font.setWeight( 0 )
    for c in range( self.COLUMN_COUNT ):
      item.setFont( c, font )

  def buildFlat(self):
    for data in self.model.filteredItems():
      item = QtGui.QTreeWidgetItem( self )
      item.setData( 0, QtCore.Qt.UserRole, data.id )

      split = data.path.split('/')
      dirname = split[-1]
      if os.path.isdir( data.path ):
        if not dirname: dirname = split[-2]
        dirname += '/'

      item.setText( self.NAME, dirname )
      item.setFlags( item.flags() | QtCore.Qt.ItemIsUserCheckable )
      item.setCheckState( 0, QtCore.Qt.Unchecked )
      self.buildRow( data, item )
      self.setItemColors( item )

  def buildTree(self):
    dataTree = PathTreeBuilder.build( self.model.path, self.model.filteredItems() )
    for name, children in dataTree.items():
      self.buildTreeChildren( self, name, children )
    self.expandAll()

  def buildTreeChildren(self, parent, name, tree):
    '''tree: SvnItem or dict '''
    if tree and hasattr( tree, 'path' ):
      isdir = os.path.isdir( tree.path )
    else:
      isdir = False

    item = QtGui.QTreeWidgetItem( parent )
    isleaf = type(tree) != dict

    if not isleaf or isdir:
      # Note: dir can be a leaf.
      name += '/'

    if isleaf:
      item.setData( 0, QtCore.Qt.UserRole, tree.id )

    item.setText( self.NAME, name )
    item.setFlags( item.flags() | QtCore.Qt.ItemIsUserCheckable )
    item.setCheckState( 0, QtCore.Qt.Unchecked )

    if isleaf:
      self.buildRow( tree, item )
      self.setItemColors( item )
      return

    item.setFlags( item.flags() | QtCore.Qt.ItemIsTristate )
    childStatus = None
    for name, children in tree.items():
      self.buildTreeChildren( item, name, children )
      if not childStatus:
        childStatus = PathTreeBuilder.findChildrenStatus( children )
        
    print item.text( self.NAME ), childStatus
    item.setText( self.STATUS, childStatus )
    self.setItemColors( item )

  def checkAll(self):
    self.checkAllChildren( self.invisibleRootItem() )

  def checkAllChildren(self, root):
    count = root.childCount()
    if not count:
      if root.isHidden():
        return
      root.setCheckState( 0, QtCore.Qt.Checked )

    for i in range( count ):
      child = root.child( i )
      self.checkAllChildren( child )

  def checked(self):
    '''Return all checked items.'''
    return self.checkedChildren( self.invisibleRootItem() )

  def checkedChildren(self, root):
    result = []
    for i in range( root.childCount() ):
      child = root.child( i )
      if child.checkState( 0 ) == QtCore.Qt.Checked:
        result.append( child )
      result.extend( self.checkedChildren( child ) )
    return result

  def checkedAndVisible(self):
    '''Return all checked items.'''
    return self.checkedAndVisibleChildren( self.invisibleRootItem() )

  def checkedAndVisibleChildren(self, root):
    result = []
    for i in range( root.childCount() ):
      child = root.child( i )
      if child.checkState( 0 ) == QtCore.Qt.Checked and not child.isHidden():
        result.append( child )
        
      result.extend( self.checkedAndVisibleChildren( child ) )
      
    return result

  def checkedIds(self):
    items = self.checked()
    return [self.getTreeWidgetItemId( item ) for item in items]

  def checkedPaths(self):
    '''Return list of paths for all checked items.'''
    items = self.checked()
    return [ str( item.text( self.PATH ) ) for item in items]

  def checkSelected(self):
    for item in self.selectedItems():
      item.setCheckState( 0, QtCore.Qt.Checked )

  def collapseSelected(self, event):
    for index in self.selectionModel().selectedIndexes():
      self.collapse( index )

  def contextMenuEvent(self, event):
    self.contextMenu.show( event )

  def copyPathsToClipboard(self, event=None):
    clipboard = QtGui.QApplication.clipboard()
    text = '\n'.join( self.selectedPaths() )
    clipboard.setText( text )

  def countChecked(self):
    return len( self.checked() )

  def countCheckedAndVisible(self):
    return len( self.checkedAndVisible() )

  def countHidden(self):
    return self.countHiddenChildren( self.invisibleRootItem() )

  def countHiddenChildren(self, root):
    result = 0
    for i in range( root.childCount() ):
      child = root.child( i )
      if child.isHidden():
        result += 1
      result += self.countHiddenChildren( child )
    return result

  def countSelected(self):
    return len( self.selectedItems() )

  def deleteSelected(self, event):
    self.svn.delete( self.selectedPaths() )
    self.updateView()

  def diffChecked(self):
    for path in self.checkedPaths():
      self.diffPath( path )

  def diffPath(self, path):
    cmd = 'svn diff --diff-cmd %s %s' % ( App.instance.cfg.settings.doubleClickDiff(), path )
    system( cmd, block=False )

  def diffSelected(self):
    for path in self.selectedPaths():
      self.diffPath( path )

  def doubleClickedSlot(self, item, column):
    self.diffPath( item.text( SvnTreeWidget.PATH ) )

  def expandSelected(self, event):
    for index in self.selectionModel().selectedIndexes():
      self.expand( index )

  def fitColumns(self):
    for i in range( self.COLUMN_COUNT ):
      self.resizeColumnToContents( i )

  def flatToggled(self, arg):
    self.flat = bool(arg)
    self.updateView()

  def getTreeWidgetItemId(self, child):
    idOk = child.data( 0, QtCore.Qt.UserRole ).toInt()
    if idOk and idOk[1]:
      return idOk[0]
    return None
    
  def hiddenIds(self):
    '''Return all hidden svn items.'''
    return self.hiddenChildrenIds( self.invisibleRootItem() )

  def hiddenChildrenIds(self, root):
    result = []
    for i in range( root.childCount() ):
      child = root.child( i )
      if child.isHidden():
        childId = self.getTreeWidgetItemId( child )
        if childId is not None:
          result.append( childId )
      result.extend( self.hiddenChildrenIds( child ) )
    return result

  def hideChecked(self):
    self.hideCheckedChildren( self.invisibleRootItem() )
    # Second pass hide directory items if all children hidden.
    self.hideParentIfAllCheckedChildrenHidden( self.invisibleRootItem() )
    self.win.hidden.addHiddenIds( self.hiddenIds() )

  def hideCheckedChildren(self, root):
    for i in range( root.childCount() ):
      child = root.child( i )
      if child.checkState( 0 ) == QtCore.Qt.Checked and child.childCount() == 0:
        child.setHidden( True )
      self.hideCheckedChildren( child )

  def hideParentIfAllCheckedChildrenHidden(self, root):
    count = root.childCount()
    if count < 1:
      return

    nHidden = 0
    for i in range( count ):
      child = root.child( i )
      self.hideParentIfAllCheckedChildrenHidden( child )
      if child.isHidden():
        nHidden += 1

    if nHidden == count:
      root.setHidden( True )

  def invertChecks(self):
    self.invertChecksChildren( self.invisibleRootItem() )

  def invertChecksChildren(self, root):
    count = root.childCount()
    if not count:
      if root.checkState( 0 ) == QtCore.Qt.Checked:
        root.setCheckState( 0, QtCore.Qt.Unchecked )
      else:
        root.setCheckState( 0, QtCore.Qt.Checked )

    for i in range( count ):
      child = root.child( i )
      self.invertChecksChildren( child )

  def meldPath(self, path):
    cmd = 'svn diff --diff-cmd %s %s' % ( App.instance.cfg.settings.meld(), path )
    system( cmd, block=False )

  def meldSelected(self):
    for path in self.selectedPaths():
      self.meldPath( path )

  def moveToGroup(self):
    ids = []
    for item in self.selectedItems():
      item.setHidden( True )
      ids.append( self.getTreeWidgetItemId( item ) )
      
    items = self.model.getItemsById( ids )
    self.win.hidden.addHiddenIds( ids )

    group = self.sender().data().toString()
    self.win.groups.add( group, items )
    
  def refresh(self, event):
    self.model.setRemote( False )
    self.updateView()

  def refreshRemote(self, event):
    self.model.setRemote( True )
    self.updateView()

  def revertSelected(self, event):
    paths = [ str( item.text( self.PATH ) ) for item in self.selectedItems() ]
    self.svn.revert( paths )

  def selectedIds(self):
    return [ self.getTreeWidgetItemId( item ) for item in self.selectedItems() ]
        
  def selectedPaths(self):
    return [ str( item.text( self.PATH ) ) for item in self.selectedItems() ]

  def setColor(self):
    p = self.palette()
    color = ColorConfig.hexToQColor( App.instance.cfg.colors.getTheme()['general']['bg'] )
    p.setColor( QtGui.QPalette.Base, color )
    self.setPalette( p )

  def setItemColors(self, item):
    style = App.instance.cfg.colors.getTheme()['svn'][ str(item.text( self.STATUS )) ]
    fgbrush = QtGui.QBrush( ColorConfig.hexToQColor( style['fg'] ) )

    for i in range( self.COLUMN_COUNT ):
      item.setForeground( i, fgbrush )

  def showAll(self):
    return self.showOnlyCheckedChildren( self.invisibleRootItem(), None )

  def showOnlyChecked(self):
    return self.showOnlyCheckedChildren( self.invisibleRootItem(), True )

  def showOnlyUnchecked(self):
    return self.showOnlyCheckedChildren( self.invisibleRootItem(), False )

  def showOnlyCheckedChildren(self, root, checked ):
    count = root.childCount()
    for i in range( count ):
      child = root.child( i )
      if checked is None:
        hide = False
      else:
        state = child.checkState( 0 )
        if checked:
          hide = (state == QtCore.Qt.Unchecked)
        else:
          hide = (state == QtCore.Qt.Checked)
      child.setHidden( hide )
      self.showOnlyCheckedChildren( child, checked )

  def uncheckAll(self):
    self.uncheckAllChildren( self.invisibleRootItem() )

  def uncheckAllChildren(self, root):
    count = root.childCount()
    if not count:
      root.setCheckState( 0, QtCore.Qt.Unchecked )

    for i in range( count ):
      child = root.child( i )
      self.uncheckAllChildren( child )

  def uncheckSelected(self):
    for item in self.selectedItems():
      item.setCheckState( 0, QtCore.Qt.Unchecked )

  def updateView(self):
    self.model.refresh()
    self.clear()
    self.setColor()
    self.build()
    self.fitColumns()

  def visibleIds(self):
    '''Return all visible svn items.'''
    return self.visibleChildrenIds( self.invisibleRootItem() )

  def visibleChildrenIds(self, root):
    result = []
    for i in range( root.childCount() ):
      child = root.child( i )
      if not child.isHidden():
        childId = self.getTreeWidgetItemId( child )
        if childId is not None:
          result.append( childId )
          
      result.extend( self.visibleChildrenIds( child ) )

    return result

  #------------------------------------------------------------------
  # View Menu Handlers
  def openPath(self, event):
    items = self.selectedItems()
    if not items:
      return

    path = str(items[0].text( self.PATH ) )
    url = QtCore.QUrl.fromLocalFile( path )
    QtGui.QDesktopServices.openUrl( url )

  #------------------------------------------------------------------
  # Repository Menu Handlers
  def doRepoAdd(self, event):
    items = self.checkedPaths()
    stdout,rcode = self.svn.add( items )
    self.win.console.appendSvnOutput( stdout )

    if len( items ):
      self.updateView()

  def doRepoCommit(self, event):
    win = CommitWindow( self.win )
    win.exec_()

    msg = str( win.textEdit.toPlainText() )
    paths = [item.path for item in win.items]
    if not paths:
      return
      
    stdout, rcode = self.svn.commit( paths, msg )
    self.win.console.appendSvnOutput( stdout )
    if rcode:
      QtGui.QMessageBox.critical( None, 'Commit Error', stdout )
      return
      
    App.instance.cfg.commits.addMessage( msg )
    
    if paths:
      self.updateView()

  def doRepoDelete(self, event):
    items = self.checkedPaths()
    stdout,rcode = self.svn.delete( items )
    self.win.console.appendSvnOutput( stdout )

    if len( items ):
      self.updateView()

  def doRemove(self, event):
    items = self.checkedPaths()
    stdout = Tools.remove( items )
    self.win.console.appendColorText( stdout, red=['Error'] )

    if len( items ):
      self.updateView()

  def doRepoUpdate(self, event):
    items = self.checkedPaths()
    stdout,rcode = self.svn.update( items )
    self.win.console.appendSvnOutput( stdout )

    if len( items ):
      self.updateView()

  #------------------------------------------------------------------
  # Ignore Menu Handlers
  def ignoreCaller(self, func):
    items = self.checked()
    for item in items:
      path = str( item.text( self.PATH ) )
      func( path )
    if len( items ):
      self.updateView()

  def ignoreAbsPath(self, event):
    self.ignoreCaller( App.instance.cfg.ignores.addAbsPath )

  def ignoreBasename(self, event):
    self.ignoreCaller( App.instance.cfg.ignores.addBaseName )

  def ignoreExtension(self, event):
    self.ignoreCaller( App.instance.cfg.ignores.addExtension )

  def ignoreDir(self, event):
    self.ignoreCaller( App.instance.cfg.ignores.addDir )

  def ignoreParentDir(self, event):
     self.ignoreCaller( App.instance.cfg.ignores.addParentDir )

#====================================================================
class Format(object):
  def __init__(self, fg='000000', bg='ffffff', weight=None, size=12):
    self.fg = fg
    self.bg = bg
    self.weight = weight
    self.size = size

  def toDict(self):
    return {
      'bg': self.bg,
      'fg': self.fg,
      'size': self.size,
      'weight': self.weight
    }

  def __str__(self):
    return str( self.toDict() )

#====================================================================
class AppConfig(object):
  def __init__(self, browsePath):
    self.browsePath = browsePath
    self.svnDir = self.findSvnDir( self.browsePath )

    self.colors = None
    self.ignores = None
    self.commits = None
    self.settings = None
    self.load()

  def configDir(self):
    # Remove Windows drive colon.
    svnDir = self.svnDir.replace(':', '')
    parts = [self.userDir(), self.dirName(), svnDir]
    result = normalizePath( os.path.sep.join( parts ) )
    
    return result

  @staticmethod
  def dirName():
    return '.' + App.name()

  @staticmethod
  def ensureCacheDirExists(path):
    if not os.path.exists(path):
      return False

  @staticmethod
  def findSvnDir(path):
    dirName = '.svn'
    while path:
      if dirName in os.listdir( path ):
        break
      split = path.rsplit( os.path.sep, 1 )
      if len(split) > 1:
        path = split[0]
      else:
        break
    return path

  def getGeneralTextFormat(self):
    fmt = QtGui.QTextFormat()
    fmt.setBackground( QtGui.QBrush( self.colors.hexToQColor( self.colors.data['general']['bg'] ) ) )
    fmt.setForeground( QtGui.QBrush( self.colors.hexToQColor( self.colors.data['general']['fg'] ) ) )
    return fmt

  def load(self):
    cfgDir = self.configDir()
    self.loadIgnores( cfgDir )
    self.loadColors( cfgDir )
    self.loadCommits( cfgDir )
    self.loadSettings( cfgDir )

  def loadColors(self, cfgDir):
    self.colors = ColorConfig( cfgDir )

  def loadIgnores(self, cfgDir):
    self.ignores = IgnoresConfig( cfgDir )

  def loadCommits(self, cfgDir):
    self.commits = CommitConfig( cfgDir )

  def loadSettings(self, cfgDir):
    self.settings = SettingsConfig( cfgDir )

  @staticmethod
  def userDir():
    from os.path import expanduser
    return expanduser("~")

#====================================================================
class JsonConfig(object):
  def __init__(self, cfgDir):
    self.cfgDir = cfgDir
    self.data = {}
    if not os.path.exists( self.path() ):
      self.createDefault()
    else:
      self.load()

  def path(self):
    return self.cfgDir + os.path.sep + self.filename()

  def createDefault(self):
    if not os.path.exists( self.cfgDir ):
      os.makedirs( self.cfgDir )

    self.data = self.defaults()
    self.store()

  def store(self):
    if not self.data:
      return
      
    import json
    with open( self.path(), 'w' ) as f:
      f.write( json.dumps( self.data, indent=2 ) )

  def load(self):
    import json
    with open( self.path() ) as f:
      try:
        self.data = json.loads( f.read() )
      except:
        print 'Error: Failed to load ' + self.path()
        
#====================================================================
class CommitConfig(JsonConfig):
  '''Logs commit messages which can be re-used for frequent tasks.'''
  def __init__(self, cfgDir):
    super( CommitConfig, self ).__init__( cfgDir )

  def addMessage(self, text):
    msgs = self.data['messages']
    if text in msgs:
      return
      
    msgs.insert( 0, text )   
    self.store()
    
  @staticmethod
  def defaults():
    return {'messages': ['My initial commit.']}

  @staticmethod
  def filename():
    return 'commits.json'

  def getMessages(self):
    return self.data['messages']
    
#====================================================================
class ColorConfig(JsonConfig):
  def __init__(self, cfgDir):
    super( ColorConfig, self ).__init__( cfgDir )
    self.theme = 'dark'

  def getTheme(self):
    '''Return dict for current theme.'''
    return self.data[ self.theme ]

  def setTheme(self, theme):
    self.theme = theme

  @staticmethod
  def defaults():
    return {
      'dark': ColorConfig.dark(),
      'light': ColorConfig.light(),
    }

  @staticmethod
  def dark():
    return {
      'general': {
        'bg': '2b2b2b',
        'fg': 'dddddd'
      },
      'svn': {
        '':            Format( fg='dddddd' ).toDict(),
        'added':       Format( fg='619647' ).toDict(),
        'conflicted':  Format( fg='cb772f' ).toDict(),
        'deleted':     Format( fg='d25252' ).toDict(),
        'external':    Format( fg='bed6ff' ).toDict(),
        'incomplete':  Format( fg='ffC66d' ).toDict(),
        'missing':     Format( fg='ffC66d' ).toDict(),
        'modified':    Format( fg='6897bb' ).toDict(),
        'none':        Format( fg='88aa55' ).toDict(),
        'normal':      Format( fg='dddddd' ).toDict(),
        'renamed':     Format( fg='9876aa' ).toDict(),
        'unversioned': Format( fg='888888' ).toDict(),
      }
    }

  @staticmethod
  def light():
    return {
      'general': {
        'bg': 'f5f5f5',
        'fg': '000000'
      },
      'svn': {
        '':            Format( fg='dddddd' ).toDict(),
        'added':       Format( fg='33aa33' ).toDict(),
        'conflicted':  Format( fg='ed772f' ).toDict(),
        'deleted':     Format( fg='bb4444' ).toDict(),
        'external':    Format( fg='bed6bb' ).toDict(),
        'incomplete':  Format( fg='dda66a' ).toDict(),
        'missing':     Format( fg='dda66a' ).toDict(),
        'modified':    Format( fg='5555aa' ).toDict(),
        'none':        Format( fg='558855' ).toDict(),
        'normal':      Format( fg='000000' ).toDict(),
        'renamed':     Format( fg='9876aa' ).toDict(),
        'unversioned': Format( fg='aaaaaa' ).toDict(),
      }
    }

  @staticmethod
  def filename():
    return 'colors.json'

  @staticmethod
  def hexToQColor(text):
    r = eval( '0x'+text[0:2] )
    g = eval( '0x'+text[2:4] )
    b = eval( '0x'+text[4:6] )
    if len(text) > 6:
      a = eval( '0x'+text[6:8] )
    else:
      a = 255

    return QtGui.QColor(r,g,b,a)

#====================================================================
class IgnoresConfig(object):
  # RULES
  EXT = 0
  ABS = 1
  DIR = 2
  BASE = 3
  
  def __init__(self, cfgDir):
    self.cfgDir = cfgDir
    self.data = set()
    if not os.path.exists( self.path() ):
      self.createDefault()
    else:
      self.load()

  def add(self, item):
    '''Add pattern or absolute path.
    Entire directories can be ignored with if name ends with forward slash.
    '''
    if not item:
      return
    self.data.add( item )

  def addAbsPath(self, item):
    self.data.add( normalizePath( item ) )
    self.store()

  def addBaseName(self, item):
    self.data.add( os.path.basename( item ) )
    self.store()

  def addDir(self, item):
    path = normalizePath( item )
    print path
    if os.path.isdir( path ):
      path = path.split('/')[-1]
    elif os.path.isfile( path ):
      path = path.split('/')[-2]
    else:
      return
    print path
    self.data.add( path + '/' )
    self.store()

  def addParentDir(self, item):
    path = normalizePath( item )
    if os.path.isdir( path ):
      split = path.split('/')[-2:]
    elif os.path.isfile( path ):
      split = path.split('/')[-3:-1]
    else:
      return
    self.data.add( '/'.join( split ) + '/' )
    self.store()

  def addExtension(self, item):
    '''Example: item=foo/tmp.png'''
    ext = os.path.splitext( item )[-1]
    if not ext:
      return
    self.data.add( '*' + ext )
    self.store()

  def createDefault(self):
    if not os.path.exists( self.cfgDir ):
      os.makedirs( self.cfgDir )

    self.data = set([ '.svn', '.git', '*.o', '*.pyc', '*.pyo', '*~', '*.so', '*.a', '*.dylib'])
    self.store()

  @staticmethod
  def filename():
    return 'ignores.txt'

  def filterPaths(self, paths):
    '''Return paths not ignored.'''
    result = []
    for path in paths:
      if not self.isIgnorable( path ):
        result.append( path )

    return result

  def isIgnorable(self, path):
    '''Check against all stored patterns and paths.
       Return None if not ignorable, RULE if is.
    '''
    ext = os.path.splitext( path )[-1]
    basename = os.path.basename( path )
    
    for rule in self.data:
      if '*' + ext == rule:
        return self.EXT, rule
      elif path == rule:
        return self.ABS, rule
      elif rule.endswith('/') and '/'+rule in path:
        return self.DIR, rule
      elif basename and basename == rule:
        return self.BASE, rule

    return None

  def load(self):
    comments = []
    lines = []
    with open( self.path() ) as f:
      for line in f.read().split( '\n' ):
        if line.lstrip().startswith('#'):
          comments.append( line )
        elif line:
          lines.append( line )
          
    self.data = set( lines )

  def path(self):
    return self.cfgDir + os.path.sep + self.filename()

  def store(self):
    with open( self.path(), 'w' ) as f:
      f.write( '\n'.join( sorted( self.data ) ) )

#====================================================================
class SettingsConfig(JsonConfig):
  DOUBLE_CLICK_DIFF = 'double-click-diff'
  DIFF = 'diff'
  MELD = 'meld'

  @staticmethod
  def defaults():
    return {
      SettingsConfig.DOUBLE_CLICK_DIFF: 'tkdiff',
      SettingsConfig.DIFF: 'tkdiff',
      SettingsConfig.MELD: '/Applications/Meld.app/Contents/MacOS/Meld',
    }

  @staticmethod
  def filename():
    return 'settings.json'

  def diff(self):
    return self.data[ SettingsConfig.DIFF ]

  def doubleClickDiff(self):
    return self.data[ SettingsConfig.DOUBLE_CLICK_DIFF ]

  def meld(self):
    return self.data[ SettingsConfig.MELD ]

#====================================================================
class MenuBar(object):
  def __init__(self, win):
    self.win = win
    self.initMenus()

  def initMenus(self):
    self.menubar = self.win.menuBar()
    self.initFile()
    self.initEdit()
    self.initView()
    self.initRepository()
    self.initGroup()
    self.initSettings()

  def initFile(self):
    exitAction = QtGui.QAction( QtGui.QIcon('exit.png'), '&Exit', self.menubar)
    exitAction.setShortcut( 'Ctrl+Q' )
    exitAction.setStatusTip( 'Exit application' )
    exitAction.triggered.connect( QtGui.qApp.quit )

    menu = self.menubar.addMenu( '&File' )
    menu.addAction( exitAction )

  def initEdit(self):
    menu = self.menubar.addMenu( '&Edit' )

    action = QtGui.QAction( '&Check All', self.menubar)
    action.triggered.connect( self.win.svnTree.checkAll )
    menu.addAction( action )

    action = QtGui.QAction( '&Uncheck All', self.menubar)
    action.triggered.connect( self.win.svnTree.uncheckAll )
    menu.addAction( action )

    action = QtGui.QAction( '&Invert Checks', self.menubar)
    action.triggered.connect( self.win.svnTree.invertChecks )
    menu.addAction( action )

    action = QtGui.QAction( 'Check &Selected', self.menubar)
    action.triggered.connect( self.win.svnTree.checkSelected )
    menu.addAction( action )

    action = QtGui.QAction( 'Uncheck Selected', self.menubar)
    action.triggered.connect( self.win.svnTree.uncheckSelected )
    menu.addAction( action )

  def initView(self):
    menu = self.menubar.addMenu( '&View' )

    action = QtGui.QAction( '&Flat', self.menubar)
    action.setCheckable( True )
    action.triggered.connect( self.win.svnTree.flatToggled )
    menu.addAction( action )
    self.flatAction = action

    #------------------------------------------------------
    menu.addSeparator()

    action = QtGui.QAction( '&Check All', self.menubar)
    action.triggered.connect( self.win.svnTree.checkAll )
    menu.addAction( action )

    action = QtGui.QAction( '&Uncheck All', self.menubar)
    action.triggered.connect( self.win.svnTree.uncheckAll )
    menu.addAction( action )

    #------------------------------------------------------
    menu.addSeparator()

    action = QtGui.QAction( 'Show &All', self.menubar)
    action.triggered.connect( self.win.svnTree.showAll )
    menu.addAction( action )

    action = QtGui.QAction( 'Show Only &Checked', self.menubar)
    action.triggered.connect( self.win.svnTree.showOnlyChecked )
    menu.addAction( action )

    action = QtGui.QAction( 'Show Only &Unchecked', self.menubar)
    action.triggered.connect( self.win.svnTree.showOnlyUnchecked )
    menu.addAction( action )

    #------------------------------------------------------
    menu.addSeparator()

    action = QtGui.QAction( 'Hide Checked', self.menubar)
    action.triggered.connect( self.win.svnTree.hideChecked )
    menu.addAction( action )

    action = QtGui.QAction( 'Hidden', self.menubar)
    action.setCheckable( True )
    action.triggered.connect( self.win.hiddenToggled )
    menu.addAction( action )

    action = QtGui.QAction( 'Ignored', self.menubar)
    action.setCheckable( True )
    action.triggered.connect( self.win.ignoredToggled )
    menu.addAction( action )
    self.viewIgnoredAction = action
    
    #------------------------------------------------------
    menu.addSeparator()

    action = QtGui.QAction( 'Expand', self.menubar)
    action.triggered.connect( self.win.svnTree.expandSelected )
    menu.addAction( action )

    action = QtGui.QAction( 'Expand All', self.menubar)
    action.triggered.connect( self.win.svnTree.expandAll )
    menu.addAction( action )

    action = QtGui.QAction( 'Collapse', self.menubar)
    action.triggered.connect( self.win.svnTree.collapseSelected )
    menu.addAction( action )

    action = QtGui.QAction( 'Collapse All', self.menubar)
    action.triggered.connect( self.win.svnTree.collapseAll )
    menu.addAction( action )

    #------------------------------------------------------
    menu.addSeparator()

    action = QtGui.QAction( 'Console', self.menubar)
    action.setCheckable( True )
    action.triggered.connect( self.win.consoleToggled )
    menu.addAction( action )
    self.viewConsoleAction = action

  def initRepository(self):
    menu = self.menubar.addMenu( '&Repository' )

    action = QtGui.QAction( '&Refresh', self.menubar)
    action.triggered.connect( self.win.svnTree.refresh )
    menu.addAction( action )

    action = QtGui.QAction( '&Refresh Remote', self.menubar)
    action.triggered.connect( self.win.svnTree.refreshRemote )
    menu.addAction( action )

    #------------------------------------------
    menu.addSeparator()

    action = QtGui.QAction( '&Add', self.menubar)
    action.triggered.connect( self.win.svnTree.doRepoAdd )
    menu.addAction( action )

    action = QtGui.QAction( '&Commit', self.menubar)
    action.triggered.connect( self.win.svnTree.doRepoCommit )
    menu.addAction( action )

    action = QtGui.QAction( '&Delete', self.menubar)
    action.triggered.connect( self.win.svnTree.doRepoDelete )
    menu.addAction( action )

    action = QtGui.QAction( '&Remove from Disk', self.menubar)
    action.triggered.connect( self.win.svnTree.doRemove )
    menu.addAction( action )

    action = QtGui.QAction( '&Update', self.menubar)
    action.triggered.connect( self.win.svnTree.doRepoUpdate )
    menu.addAction( action )

    #------------------------------------------
    menu.addSeparator()
    action = QtGui.QAction( '&Ignore Absolute Path', self.menubar)
    action.setToolTip( 'Such as "C:\my\file.txt", or "C:\my\dir". For all checked items.' )
    action.triggered.connect( self.win.svnTree.ignoreAbsPath )
    menu.addAction( action )

    action = QtGui.QAction( '&Ignore Base Name', self.menubar)
    action.setToolTip( 'Such as "foo" for item like "C:\foo.txt". For all checked items.' )
    action.triggered.connect( self.win.svnTree.ignoreBasename )
    menu.addAction( action )

    action = QtGui.QAction( '&Ignore Extension', self.menubar )
    action.triggered.connect( self.win.svnTree.ignoreExtension )
    menu.addAction( action )

    action = QtGui.QAction( '&Ignore Dir', self.menubar)
    action.triggered.connect( self.win.svnTree.ignoreDir )
    menu.addAction( action )

    action = QtGui.QAction( '&Ignore Parent/Dir', self.menubar)
    action.triggered.connect( self.win.svnTree.ignoreParentDir )
    menu.addAction( action )

    #-----------------------------------------------------------------
    menu.addSeparator()
    # Diff Checked, Diff Selected

    # Update
    # Ignore
    # Revert
    pass

  def initGroup(self):
    menu = self.menubar.addMenu( '&Group' )

    action = QtGui.QAction( '&New...', self.menubar)
    action.triggered.connect( self.win.groups.askForName )
    menu.addAction( action )

  def initSettings(self):
    menu = self.menubar.addMenu( '&Settings' )
    #-----------------------------------------------------------------
    action = QtGui.QAction( 'Dark Theme', self.menubar)
    action.triggered.connect( self.showDarkTheme )
    action.setCheckable( True )
    action.setChecked( True )
    menu.addAction( action )
    self.darkAction = action

    action = QtGui.QAction( 'Light Theme', self.menubar)
    action.triggered.connect( self.showLightTheme )
    action.setCheckable( True )
    menu.addAction( action )
    self.lightAction = action

    #-----------------------------------------------------------------
    menu.addSeparator()

    action = QtGui.QAction( '&Colors File', self.menubar)
    action.triggered.connect( self.showColorsFile )
    menu.addAction( action )

    action = QtGui.QAction( '&Ignores File', self.menubar)
    action.triggered.connect( self.showIgnoresFile )
    menu.addAction( action )

    action = QtGui.QAction( '&General Settings File', self.menubar)
    action.triggered.connect( self.showSettingsFile )
    menu.addAction( action )

    #-----------------------------------------------------------------
    menu.addSeparator()

    action = QtGui.QAction( 'Reset Colors File', self.menubar)
    action.triggered.connect( self.resetColorsFile )
    menu.addAction( action )

  def resetColorsFile(self, event):
    App.instance.cfg.colors.createDefault()
    self.win.updateViews()

  def showColorsFile(self, event):
    print App.instance.cfg.colors.path()

  def showDarkTheme(self, event):
    App.instance.cfg.colors.setTheme( 'dark' )
    self.darkAction.setChecked( True )
    self.lightAction.setChecked( False )
    self.win.updateViews()

  def showLightTheme(self, event):
    App.instance.cfg.colors.setTheme( 'light' )
    self.lightAction.setChecked( True )
    self.darkAction.setChecked( False )
    self.win.updateViews()

  def showIgnoresFile(self, event):
    print App.instance.cfg.ignores.path()

  def showSettingsFile(self, event):
    print App.instance.cfg.settings.path()

#====================================================================
class StatusBar(object):
  def __init__(self, win):
    self.win = win

    self.timer = QtCore.QTimer()
    self.timer.timeout.connect( self.update )
    # check half second
    self.timer.start(500)

  def update(self):
    hidden = self.win.svnTree.countHidden()
    selected = self.win.svnTree.countSelected()
    checked = self.win.svnTree.countCheckedAndVisible()
    gap = '    '
    fmt = "Hidden: {0}{1}Selected: {2}{3}Checked: {4}"
    text = fmt.format( hidden, gap, selected, gap, checked )
    self.win.statusBar().showMessage( text )

#====================================================================
class ConsoleView(QtGui.QTextEdit):
  _sizehint = None
  
  def __init__(self, parent):
    super( ConsoleView, self ).__init__( parent )
    self.setColor()
    self.setFont( Tools.getMonoFont() )
    
  def appendPlainText(self, text):
    html = self.toHtml()
    lines = ['<pre>'+ t +'</pre>' for t in text.split('\n')]
    html += '<p><font color=grey>' + '<br>'.join( lines ) + '</font></p>'
    self.setHtml( html )
    self.setColor()
    
  def appendColorText(self, text, red=[]):
    html = self.toHtml()
    html += self.colorizeText( text, red )
    self.setHtml( html )
    self.setColor()

  def appendSvnOutput(self, text):
    '''Colorize text output from `svn update` '''
    html = self.toHtml()
    html += self.svnOutputToHtml( text )
    self.setHtml( html )
    self.update()
    self.setColor()

  @staticmethod
  def colorizeText(text, red=[]):
    out = ['<p>']
    for line in text.split('\n'):
      if not line:
        continue

      s = line
      found = False
      for pattern in red:
        if pattern in line:
          s = '<font color="red" face="monospace">{0}</font>'.format( line )
          found = True
          break
        
      if not found:
        s = '<font color=grey face="monospace">{0}</font>'.format( line )

      out.append( s )
      out.append( '<br>' )

    out.append( '</p>' )

    return ''.join( out )

  @staticmethod
  def svnOutputToHtml(text):
    out = ['<p>']
    for line in text.split('\n'):
      if not line:
        continue

      s = line
      if line[0] == 'A':
        s = '<font color="green" face="monospace">{0}</font>'.format( line )
      if line[0] == 'D':
        s = '<font color="orange" face="monospace">{0}</font>'.format( line )
      elif line.startswith('svn: E'):
        s = '<font color="red" face="monospace">{0}</font>'.format( line )
      else:
        s = '<font color=grey face="monospace">{0}</font>'.format( line )

      out.append( s )
      out.append( '<br>' )

    out.append( '</p>' )

    return ''.join( out )

  def setColor(self):
    p = self.palette()
    bg = ColorConfig.hexToQColor( App.instance.cfg.colors.getTheme()['general']['bg'] )
    p.setColor( QtGui.QPalette.Base, bg )
    self.setPalette( p )

    fg = ColorConfig.hexToQColor( App.instance.cfg.colors.getTheme()['general']['fg'] )
    self.setTextColor( fg )    
  
  def setSizeHint(self, width, height):
    self._sizehint = QtCore.QSize(width, height)

  def sizeHint(self):
    if self._sizehint is not None:
      return self._sizehint
    return super(ConsoleView, self).sizeHint()
        
  def updateView(self):
    self.setColor()
    
#====================================================================
class HiddenItemsView(ItemListView):
  def __init__(self, parent):
    super( HiddenItemsView, self ).__init__( parent )
    self.hiddenItems = {}
    self.model = None

  def addHiddenIds(self, ids):
    items = self.model.getItemsById( ids )
    for item in items:
      self.hiddenItems[ item.id ] = item

    self.updateView()

  def getItems(self):
    paths = [item.path for item in self.hiddenItems.values()]
    paths.sort()
    
    return paths

  def setModel(self, model):
    self.model = model

#====================================================================
class IgnoredItemsView(ItemListView):
  def __init__(self, parent):
    super( IgnoredItemsView, self ).__init__( parent )
    self.model = None

    font = QtGui.QFont( "Monospace" )
    font.setStyleHint( QtGui.QFont.TypeWriter )
    self.setFont( font )
    
  def getItems(self):
    result = []
    prefixLen = len(self.model.path)+1 # Plus one for slash.
    paths = ['Path']
    rules = ['Rule']
    
    for pathRule in self.model.ignoredItems():
      path = pathRule[0]
      shortPath = path[ prefixLen: ]
      paths.append( shortPath )
      rules.append( pathRule[-1][-1] ) 
   
    # Find max path to justify rules with pad.
    maxlen = 0
    for p in paths:
      if len(p) > maxlen: 
        maxlen = len(p)
        
    for i in range(len(paths)):
      pad = ' ' * ( maxlen - len(paths[i]) + 2 )
      s = '{0}{1}{2}'.format( paths[i], pad, rules[i] )
      result.append( s )
      
    return result
    
  def setModel(self, model):
    self.model = model
    
#====================================================================
class MainWindow(QtGui.QMainWindow):
  def __init__(self):
    super( MainWindow, self ).__init__()
    self.initUi()

  def center(self):
    qr = self.frameGeometry()
    cp = QtGui.QDesktopWidget().availableGeometry().center()
    qr.moveCenter(cp)
    self.move( qr.topLeft() )

  def consoleToggled(self):
    self.consoleDock.setVisible( not self.consoleDock.isVisible() )

  def hiddenToggled(self):
    self.hiddenDock.setVisible( not self.hiddenDock.isVisible() )

  def ignoredToggled(self):
    self.ignoredDock.setVisible( not self.ignoredDock.isVisible() )
    self.ignored.updateView()
    
  def initUi(self):
    self.setGeometry( 200, 200, 1600, 1000 )

    self.initWidgets()
    self.initMenu()

    self.center()
    self.setWindowTitle( App.name() )
    self.showSvnTree()
    self.show()

  def initMenu(self):
    self.menubar = MenuBar( self )

  def initWidgets(self):
    self.svnTree = SvnTreeWidget( self )
    self.blankWidget = None
    self.statusbar = StatusBar( self )
    self.initConsole()
    self.initGroups()
    self.initHidden()
    self.initIgnored()
    
  def initConsole(self):
    self.console = ConsoleView( self )
    dock = QtGui.QDockWidget( "Console", self )
    dock.setWidget( self.console )
    dock.setVisible( False )
    self.addDockWidget( QtCore.Qt.BottomDockWidgetArea, dock )
    self.consoleDock = dock
    self.console.setSizeHint( self.geometry().width()/2, 200 )

  def initGroups(self):
    self.groups = Groups( self )
    
  def initHidden(self):
    self.hidden = HiddenItemsView( self )
    dock = QtGui.QDockWidget( "Hidden", self )
    dock.setWidget( self.hidden )
    dock.setVisible( False )
    self.addDockWidget( QtCore.Qt.BottomDockWidgetArea, dock )
    self.hiddenDock = dock

  def initIgnored(self):
    self.ignored = IgnoredItemsView( self )
    dock = QtGui.QDockWidget( "Ignored", self )
    dock.setWidget( self.ignored )
    dock.setVisible( False )
    self.addDockWidget( QtCore.Qt.BottomDockWidgetArea, dock )
    self.ignoredDock = dock
    self.ignored.setSizeHint( self.geometry().width()/2, 200 )
    
  def setModel(self, model):
    self.svnTree.setModel( model )
    self.hidden.setModel( model )
    self.ignored.setModel( model )
    
  def setSvn(self, svn):
    self.svnTree.setSvn( svn )

  def showBlank(self):
    self.blankWidget = QtGui.QLabel( self )
    p = self.blankWidget.palette()
    p.setBrush( QtGui.QPalette.Window, QtGui.QBrush( QtGui.QColor(0,0,0) ) )
    self.blankWidget.setPalette( p )

  def showSvnTree(self):
    if self.blankWidget:
      self.blankWidget.hide()

    self.setCentralWidget( self.svnTree )
    self.svnTree.show()

  def showSvnTreeInitially(self):
    '''If there are any unversioned, sort to show them at top.'''
    if self.svnTree.model.countUnversioned():
      self.svnTree.flatToggled( True )
      self.menubar.flatAction.setChecked( True )
      self.svnTree.sortByColumn( self.svnTree.STATUS, QtCore.Qt.DescendingOrder )

    self.showSvnTree()
    self.menubar.viewConsoleAction.trigger()
    self.menubar.viewIgnoredAction.trigger()
    
  def showUpToDate(self):
    label = QtGui.QLabel( self )
    label.setText( 'All files are up to date.' )
    label.setAlignment( QtCore.Qt.AlignCenter )
    #fmt = App.instance.cfg.getGeneralTextFormat()
    #label.setTextFormat( fmt )
    self.setCentralWidget( label )

  def updateViews(self):
    '''Call when all should repaint, such as color theme changed.'''
    self.console.updateView()
    self.ignored.updateView()
    self.svnTree.updateView()
    
#====================================================================
class App(object):
  instance = None

  def __init__(self, argv):
    App.instance = self
    self.app = QtGui.QApplication( argv )
    self.cli = Cli.parse( argv[1:] )
    self.cfg = AppConfig( self.cli['path'] )
    self.win = None
    self.svn = Svn()
    self.model = None

  @staticmethod
  def name(): return 'svng'

  def run(self):
    self.win = MainWindow()
    QtCore.QTimer.singleShot( 0, self.start )
    return self.app.exec_()

  def start(self):
    self.setBrowsePath( self.cli['path'] )

  def showMessage(self, text):
    self.win.statusBar().showMessage( text )

  def setBrowsePath(self, path):
    self.win.showBlank()
    App.instance.showMessage( 'Processing...' )

    self.model = Model( path )
    self.svn.setModel( self.model )
    self.win.setModel( self.model )
    self.win.setSvn( self.svn )
    App.instance.showMessage( '' )

    if self.model.isEmpty():
      self.win.showUpToDate()
    else:
      self.win.showSvnTreeInitially()

#====================================================================
class Cli(object):
  @staticmethod
  def parse(argv):
    """Subversion GUI. Forwards all command line options to svn backend."""
    from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, RawDescriptionHelpFormatter
    example = """
examples:
  %(prog)s
  %(prog)s ../src
"""
    parser = ArgumentParser(description=Cli.parse.__doc__, epilog=example,
              # formatter_class=ArgumentDefaultsHelpFormatter,
              formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument('path', nargs='?', default=None) # Positional
    args = parser.parse_args( argv )

    if not args.path:
      path = os.getcwd()
    elif os.path.isabs( args.path ):
      path = args.path
    else:
      path = os.getcwd() + os.path.sep + args.path

    result = {
      'path': Tools.findSvnDir( path )
    }
    return result

#====================================================================
def main(argv):
  app = App( argv )
  rcode = app.run()

  sys.exit( rcode )

if '__main__' == __name__:
  main( sys.argv )

